package net.ciderpunk.minimalism.ents;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import net.ciderpunk.gamebase.ents.IKillable;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.ents.weapons.Blaster;
import net.ciderpunk.minimalism.ents.weapons.MultiplexWeapon;
import net.ciderpunk.minimalism.ents.weapons.Weapon;
import net.ciderpunk.minimalism.gui.MinimalismGame;

public class Player extends MinEntity implements IKillable{

	static Frame shadow;
	static Animation idleAnim;
	static Animation leftAnim;
	static Animation rightAnim;
	static Color shadowCol;
	private Vector2 target;
	private Vector2 diff;
	protected Weapon currentWeapon;
	protected float speed;
	protected int health;
	protected Boolean firing;
	
	protected Array<Weapon> guns;  
	protected int currentGun;
	protected long score;
	
	
	
	public void increaseGun(){
		if (currentGun < guns.size - 1){
			currentGun++;
		}
	}
	
	public void decreaseGun(){
		if (currentGun > 0){
			currentGun--;
		}
	}

	Boolean active;

	public int getHealth() {
		return health;
	}

	public Player(){};
	
	public Player(MinimalismGame owner, int x, int y){
		super(owner, x,y);
		this.currentFrame = (Frame) idleAnim.getKeyFrame(0f);
		this.setAnim(idleAnim);
		firing = false;
		buildGuns();
		currentGun = 0;
		this.target = new Vector2(this.loc);
		this.diff = new Vector2();
		speed = 4.0f;
		health = 100;
		this.active = true;
		this.score = 0;
	}
	
	public void addScore(int value){
		if (this.getActive()){
			this.score += value;
		}
	}
	
	public long getScore(){
		return this.score;
	}
	
	private void buildGuns() {
		guns = new Array<Weapon>();
		
	/*	
		guns.add(new MultiplexWeapon()
		.addWeapon(new Blaster(new Vector2(-10, 6), 0f, 0.05f, 0f))
		.addWeapon(new Blaster(new Vector2(12, 6), 0f, 0.05f, 0.04f))
		.addWeapon(new Blaster(new Vector2(15, 6), -23f, 0.05f, 0.02f))
		.addWeapon(new Blaster(new Vector2(-13, 6), 23f, 0.05f, 0.03f))
		.addWeapon(new Blaster(new Vector2(15, 6), -45f, 0.05f, 0.04f))
		.addWeapon(new Blaster(new Vector2(-13, 6), 45f, 0.05f, 0.01f))
		
		.addWeapon(new Blaster(new Vector2(-10, 6), 10f, 0.05f, 0f))
		.addWeapon(new Blaster(new Vector2(12, 6), -10f, 0.05f, 0.04f))
		.addWeapon(new Blaster(new Vector2(15, 6), -16f, 0.05f, 0.02f))
		.addWeapon(new Blaster(new Vector2(-13, 6), 16f, 0.05f, 0.03f))
		.addWeapon(new Blaster(new Vector2(15, 6), -30f, 0.05f, 0.04f))
		.addWeapon(new Blaster(new Vector2(-13, 6), 30f, 0.05f, 0.01f))
	);
		*/
		
		guns.add(new MultiplexWeapon()
			.addWeapon(new Blaster(new Vector2(12, 6), 0f, 0.2f, 0.1f))
			.addWeapon(new Blaster(new Vector2(-10, 6), 0f, 0.2f, 0f))
		);
		
		guns.add(new MultiplexWeapon()
		.addWeapon(new Blaster(new Vector2(0, 6), 0f, 0.1f, 0f))
		.addWeapon(new Blaster(new Vector2(12, 6), -30f, 0.25f, 0.03f))
		.addWeapon(new Blaster(new Vector2(-10, 6), 30f, 0.25f, 0.06f))
	);
		
		guns.add(new MultiplexWeapon()
			.addWeapon(new Blaster(new Vector2(12, 6), 0f, 0.1f, 0.05f))
			.addWeapon(new Blaster(new Vector2(-10, 6), 0f, 0.1f, 0f))
		);
		

		guns.add(new MultiplexWeapon()
			.addWeapon(new Blaster(new Vector2(0, 6), 0f, 0.1f, 0f))
			.addWeapon(new Blaster(new Vector2(12, 6),- 30f, 0.1f, 0.03f))
			.addWeapon(new Blaster(new Vector2(-10, 6), 30f, 0.1f, 0.06f))
	  );
		
		guns.add(new MultiplexWeapon()
			.addWeapon(new Blaster(new Vector2(-10, 6), 0f, 0.2f, 0f))
			.addWeapon(new Blaster(new Vector2(12, 6), 0f, 0.2f, 0.04f))
			.addWeapon(new Blaster(new Vector2(15, 6), -23f, 0.2f, 0.02f))
			.addWeapon(new Blaster(new Vector2(-13, 6), 23f, 0.2f, 0.03f))
		);
		
		guns.add(new MultiplexWeapon()
			.addWeapon(new Blaster(new Vector2(-10, 6), 0f, 0.1f, 0f))
			.addWeapon(new Blaster(new Vector2(12, 6), 0f, 0.1f, 0.04f))
			.addWeapon(new Blaster(new Vector2(15, 6), -23f, 0.2f, 0.02f))
			.addWeapon(new Blaster(new Vector2(-13, 6), 23f, 0.2f, 0.03f))
		);
		
		guns.add(new MultiplexWeapon()
			.addWeapon(new Blaster(new Vector2(-10, 6), 0f, 0.1f, 0f))
			.addWeapon(new Blaster(new Vector2(12, 6), 0f, 0.1f, 0.04f))
			.addWeapon(new Blaster(new Vector2(15, 6), -23f, 0.1f, 0.02f))
			.addWeapon(new Blaster(new Vector2(-13, 6), 23f, 0.1f, 0.03f))
		);
	
		guns.add(new MultiplexWeapon()
			.addWeapon(new Blaster(new Vector2(-10, 6), 0f, 0.1f, 0f))
			.addWeapon(new Blaster(new Vector2(12, 6), 0f, 0.1f, 0.04f))
			.addWeapon(new Blaster(new Vector2(15, 6), -23f, 0.2f, 0.02f))
			.addWeapon(new Blaster(new Vector2(-13, 6), 23f, 0.2f, 0.03f))
			.addWeapon(new Blaster(new Vector2(15, 6), -45f, 0.2f, 0.04f))
			.addWeapon(new Blaster(new Vector2(-13, 6), 45f, 0.2f, 0.01f))
		);
	}

	public Boolean getActive() {
		return active;
	}

	@Override
	public void draw(SpriteBatch batch) {
		
		if (this.active){
			// TODO Auto-generated method stub
			currentFrame.draw(batch, loc.x+8, loc.y-8, Constants.ShadowColor);
			super.draw(batch);
		}
	}


	@Override
	public Boolean update(float dT)
	{
		if (active){
			if(health <= 0){
				this.getOwner().setGameOver();
				this.active = false;
				this.getOwner().addDecoration(new Explosion(this.getOwner(), this.loc));
			}
			//get distance
			diff.set(target).sub(loc);
			//if were more than a pixel away, move closer
			if (diff.len() > 5f){
				this.loc.add(diff.nor().scl(speed));
				if (diff.x > 1.0){
					this.setAnim(rightAnim);
				}
				else if (diff.x < -1.0){
					this.setAnim(leftAnim);
				}
				else{
					this.setAnim(idleAnim);
				}
			}
			else{
				this.setAnim(idleAnim);
			}
			//guns!
			guns.get(currentGun).update(this, this.firing, dT);
			//animation
			 super.update(dT);
		}
		return true;
	}


	public void startFire(){
		firing = true;
	}
	
	public void ceaseFire(){
		firing = false;
	}

	public void updateTarget(float dX, float dY) {
		target.add(dX,  dY);
		if (target.x > this.getOwner().getWidth()){
			target.x = this.getOwner().getWidth();
		}
		if (target.x < 0){
			target.x = 0;
		}
		if (target.y > this.getOwner().getHeight()){
			target.y= this.getOwner().getHeight();
		}
		if (target.y < 0){
			target.y = 0;
		}
	}

	@Override
	protected float getCollisionRadius() {
		// TODO Auto-generated method stub
		return 12f;
	}

	@Override
	public Boolean Hurt(int damage) {
		if (this.active){

			this.health += damage;
			if (this.health > 100){
				this.health = 100;
			}
			if (damage < 0){
				this.decreaseGun();
			}
			return true;
		}
		return false;
	}

	@Override
	protected void getTextures(TextureAtlas atlas) {
		idleAnim = this.buildAnim(atlas, "player_idle" , 0.025f, 16, 16);
		leftAnim = this.buildAnim(atlas, "player_left" , 0.025f, 16, 16);
		rightAnim = this.buildAnim(atlas, "player_right" , 0.025f, 16, 16);
	}
	
}
