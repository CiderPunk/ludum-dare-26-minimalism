package net.ciderpunk.minimalism.ents;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.ents.AnimatedEntity;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.gui.MinimalismGame;

public abstract class MinEntity extends AnimatedEntity implements IResourceUser {

	public MinEntity() {
		super();
	}

	public MinEntity(MinimalismGame owner, Vector2 loc) {
		super(owner,loc);
	}

	
	public MinEntity(MinimalismGame owner, int x, int y) {
		super((GuiElement) owner, x, y);
	}


	@Override
	public MinimalismGame getOwner() {
		return (MinimalismGame) super.getOwner();
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		getTextures(atlas);
	}

	protected abstract void getTextures(TextureAtlas atlas);
	
	@Override
	protected float getCollisionRadius() {
		// TODO Auto-generated method stub
		return 0;
	}

	


}
