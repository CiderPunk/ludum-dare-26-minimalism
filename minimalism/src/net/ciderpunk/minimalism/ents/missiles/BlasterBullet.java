package net.ciderpunk.minimalism.ents.missiles;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.IKillable;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.gui.MinimalismGame;

public class BlasterBullet extends Entity implements IResourceUser {

	static long soundRef;
	static Sound shootFx;
	static Sound hitFx;
	static Frame normal; 
	static final int damage = -10;
	Vector2 v;
	
	@Override
	public MinimalismGame getOwner() {
		return (MinimalismGame) super.getOwner();
	}

	
	
	public BlasterBullet(){ super(); }
	
	public BlasterBullet(MinimalismGame owner, Vector2 start, Vector2 vel) {
		super(owner, start);
		v = new Vector2(vel);
		this.currentFrame = normal;
		shootFx.stop(soundRef);
		soundRef = shootFx.play(MathUtils.random(0.6f, 1f));
	}

	@Override
	public Boolean update(float dT) {
		this.loc.add(v);
		Entity target = this.getOwner().getOpFor().pointCollisonTest(this.loc, 0f);
		if (target != null && target instanceof IKillable  && ((IKillable) target).Hurt(damage)){
			
			hitFx.stop(soundRef);
			soundRef = hitFx.play(MathUtils.random(0.2f, 0.4f));
			this.getOwner().getPlayer().addScore(1);
			//kill this bullet
			return false;
		}
		return this.getOwner().checkBounds(this.loc);
	}

	@Override
	protected float getCollisionRadius() {
		return 0f;
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load("sfx/shoot.wav", Sound.class);
		resMan.getAssetMan().load("sfx/hit.wav", Sound.class);
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		normal = new Frame(atlas.findRegion("blaster"),4,4);
		shootFx = resMan.getAssetMan().get("sfx/shoot.wav", Sound.class);
		hitFx = resMan.getAssetMan().get("sfx/hit.wav", Sound.class);
	}
	
}

	