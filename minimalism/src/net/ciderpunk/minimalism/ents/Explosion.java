package net.ciderpunk.minimalism.ents;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.minimalism.gui.MinimalismGame;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Explosion extends MinEntity {
	
	static Sound boomSfx;
	static Animation explosion;
	static long soundRef = -1;
	float scale;
	float rot;
	
	
	public Explosion() {

	}

	public Explosion(MinimalismGame owner, Vector2 loc) {
		this(owner, loc, 0, 0);
	}

	public Explosion(MinimalismGame owner, Vector2 loc, float scaleMin, float scaleMax){
		super(owner, loc);
		this.rot = MathUtils.random(360f);
		this.scale = (scaleMax == 0f ? 1f : MathUtils.random(scaleMax, scaleMin));
		this.setAnim(explosion);
		boomSfx.stop(soundRef);
		soundRef = boomSfx.play(MathUtils.random(0.4f, 0.6f));
	}

	public Boolean update(float dT) {
		this.stateTime += dT;
		this.currentFrame = (Frame)currentAnim.getKeyFrame(this.stateTime, false);
		this.loc.y += this.getOwner().getWorldSpeed();
		return this.stateTime < explosion.animationDuration;
	}

	@Override
	public boolean pointCollisoonTest(Vector2 target, float distance) {
		//no collisons with explosions
		return false;
	}

	@Override
	protected float getCollisionRadius() {
		return 0f;
	}

	@Override
	public void draw(SpriteBatch batch){
		currentFrame.draw(batch, loc.x, loc.y, this.rot, this.scale);
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		super.preLoad(resMan);
		resMan.getAssetMan().load("sfx/boom.wav", Sound.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		super.postLoad(resMan);
		boomSfx = resMan.getAssetMan().get("sfx/boom.wav", Sound.class);
	}

	@Override
	protected void getTextures(TextureAtlas atlas) {
		explosion = this.buildAnim(atlas, "explode/explode", 0.01f, 32, 32);
	}

	
}
