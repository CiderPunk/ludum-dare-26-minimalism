package net.ciderpunk.minimalism.ents.opfor;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.ents.Explosion;
import net.ciderpunk.minimalism.ents.Player;
import net.ciderpunk.minimalism.gui.MinimalismGame;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Potato extends OpFor {
	static Animation rotate;
	
	protected Vector2 dV;
	int shadowDist;
	float dR;
	float rot;

	
	public Potato(){};
	
	public Potato(MinimalismGame owner, int x, int y){
		super(owner, x,y);
		this.currentFrame = (Frame) rotate.getKeyFrame(0f);
		this.setAnim(rotate);
		this.stateTime = MathUtils.random(0f, rotate.animationDuration);
		Player player = this.getOwner().getPlayer();
		//this.dR = MathUtils.random(-1f, 1f);
		this.rot = MathUtils.random(-20f,20f);
		dV = new Vector2(player.getLoc()).sub(this.loc).rotate(MathUtils.random(-30f, 30f)).nor().scl(MathUtils.random(4f,5.5f));
		shadowDist = MathUtils.random(2, 12);
		this.health = 50;
	}

	@Override
	public void draw(SpriteBatch batch) {
		currentFrame.draw(batch, loc.x+shadowDist, loc.y-shadowDist, this.rot, Constants.ShadowColor);
		if (this.hurtTime > 0f){
			currentFrame.draw(batch, loc.x, loc.y,this.rot, Constants.DamageFlash);
		}
		else{
			currentFrame.draw(batch, loc.x, loc.y,this.rot);
		}
	}

	@Override
	public Boolean update(float dT) {
		this.loc.add(dV);
		return super.update(dT) && this.getOwner().checkBounds(this.loc, 50);
	}


	@Override
	protected float getCollisionRadius() {
		// TODO Auto-generated method stub
		return 20;
	}

	@Override
	protected Boolean doDeath() {
		this.getOwner().addDecoration( new Explosion(this.getOwner(), this.loc, 2f,3f));
		if (MathUtils.random(6) == 0){
			this.dropPower();
		}
		return false;
	}


	@Override
	protected int getScore() {
		// TODO Auto-generated method stub
		return 50;
	}

	@Override
	protected int getCollisionDamage() {
		// TODO Auto-generated method stub
		return -20;
	}

	@Override
	protected void getTextures(TextureAtlas atlas) {
		rotate = this.buildAnim(atlas, "potato1", 0.2f, 24, 24);
	}

}
