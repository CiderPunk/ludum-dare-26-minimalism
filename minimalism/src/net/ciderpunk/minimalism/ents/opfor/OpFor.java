package net.ciderpunk.minimalism.ents.opfor;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.IKillable;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.ents.MinEntity;
import net.ciderpunk.minimalism.ents.powerup.BluePower;
import net.ciderpunk.minimalism.ents.powerup.GreenPower;
import net.ciderpunk.minimalism.ents.powerup.RedPower;
import net.ciderpunk.minimalism.gui.MinimalismGame;

public abstract class OpFor extends MinEntity implements IKillable {
	int health;
	float hurtTime;
	
	public OpFor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OpFor(MinimalismGame owner, int x, int y) {
		super(owner, x, y);
		hurtTime = 0f;
		// TODO Auto-generated constructor stub
	}

	public OpFor(MinimalismGame owner, Vector2 loc) {
		super(owner, loc);
		hurtTime = 0f;
		// TODO Auto-generated constructor stub
	}

	protected void dropPower() {
		switch (MathUtils.random(4)){
			case 0:
				this.getOwner().addDecoration(new GreenPower(this.getOwner(), this.loc) );
				break;
			case 1:
				this.getOwner().addDecoration(new BluePower(this.getOwner(), this.loc) );
				break;
			default:
				this.getOwner().addDecoration(new RedPower(this.getOwner(), this.loc) );
				break;
		}
	}
	@Override
	public Boolean Hurt(int damage) {
		this.health += damage;
		this.hurtTime = Constants.hurtTime;
		return true;
	}

	@Override
	protected float getCollisionRadius() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Boolean update(float dT) {
		hurtTime -= dT;
		Entity target = this.getOwner().getPlayers().pointCollisonTest(this.loc, this.getCollisionRadius());
		if (target != null && target instanceof IKillable  && ((IKillable) target).Hurt(this.getCollisionDamage())){
			return this.doDeath();
		}
				
		if (this.health < 0){
			this.getOwner().getPlayer().addScore(this.getScore());
			//((MinimalismGame)owner).addDecoration( new Explosion(owner, this.loc, 2f,3f));
			return this.doDeath();
		}		
		return this.getOwner().checkBounds(this.loc, 100) && super.update(dT);
	}

	protected abstract Boolean doDeath();
	protected abstract int getScore();
	protected abstract int getCollisionDamage();

}
