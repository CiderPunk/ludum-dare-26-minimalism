package net.ciderpunk.minimalism.ents.powerup;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.minimalism.ents.MinEntity;
import net.ciderpunk.minimalism.ents.Player;
import net.ciderpunk.minimalism.gui.MinimalismGame;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

public abstract class Powerup extends MinEntity {

	protected static Animation red;
	protected static Animation blue;
	protected static Animation green;
	
	protected static Sound powerFx;

	
	@Override
	protected float getCollisionRadius() {
		// TODO Auto-generated method stub
		return 20f;
	}

	abstract void doPowerup(Player player);
	
	@Override
	public Boolean update(float dT) {
		Entity target = this.getOwner().getPlayers().pointCollisonTest(this.loc, this.getCollisionRadius());
		if (target != null && target instanceof Player ){
			//play sound
			powerFx.play(1.0f);
			doPowerup((Player) target);
			//kill this powerup
			return false;
		}
		//head down the screen like a motherfucker!
		this.loc.y -= 2f;
		return super.update(dT);
	}

	public Powerup() {
		super();
	}

	public Powerup(MinimalismGame owner, Vector2 loc) {
		super(owner, loc);
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		super.preLoad(resMan);
		resMan.getAssetMan().load("sfx/powerup.wav", Sound.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		// TODO Auto-generated method stub
		super.postLoad(resMan);
		powerFx = resMan.getAssetMan().get("sfx/powerup.wav", Sound.class);
	}

	@Override
	protected void getTextures(TextureAtlas atlas) {
		red = this.buildAnim(atlas, "power_red", 0.125f, 12, 12);
		blue = this.buildAnim(atlas, "power_blue", 0.125f, 12, 12);
		green = this.buildAnim(atlas, "power_green", 0.125f, 12, 12);
	}

}

