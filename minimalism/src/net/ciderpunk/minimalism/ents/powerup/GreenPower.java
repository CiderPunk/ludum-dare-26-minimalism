package net.ciderpunk.minimalism.ents.powerup;

import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.minimalism.ents.Player;
import net.ciderpunk.minimalism.gui.MinimalismGame;

public class GreenPower extends Powerup {

	public GreenPower(MinimalismGame owner, Vector2 loc) {
		super(owner, loc);
		this.setAnim(Powerup.green);
	}

	public GreenPower() {
		super();
	}

	@Override
	void doPowerup(Player player) {
		//give the dude some health
		player.Hurt(50);		
		this.getOwner().getPlayer().addScore(100);
		
	}

}
