package net.ciderpunk.minimalism.ents.powerup;

import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.minimalism.ents.Player;
import net.ciderpunk.minimalism.gui.MinimalismGame;

public class BluePower extends Powerup {

	public BluePower(MinimalismGame owner, Vector2 loc) {
		super(owner, loc);
		this.setAnim(Powerup.blue);
	}

	public BluePower() {
		super();
	}

	@Override
	void doPowerup(Player player) {
		//give the dude some points!
		this.getOwner().getPlayer().addScore(2000);
	}

}
