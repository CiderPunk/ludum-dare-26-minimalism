package net.ciderpunk.minimalism.ents.weapons;

import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.minimalism.ents.MinEntity;

public abstract class Weapon {
	
	protected Vector2 offset;
	protected Boolean wasFiring;
	float fireDelay;
	float cycleRate;
	float warmUp;
	protected Boolean cold;
	

	public float getCycleRate() {
		return cycleRate;
	}

	public void setCycleRate(float cycleRate) {
		this.cycleRate = cycleRate;
	}

	public float getWarmUp() {
		return warmUp;
	}

	public void setWarmUp(float warmUp) {
		this.warmUp = warmUp;
	}

	public Weapon(Vector2 offset, float rate, float warmUp){
		this.offset = offset; 	
		fireDelay = 0f;
		cycleRate = rate;	
		this.warmUp = warmUp;
		cold = true;
	}

	public void update(MinEntity firer, Boolean firing, float dT){
		fireDelay -= dT; 
		if (firing){
			if (cold){
				fireDelay+=this.warmUp;
				cold = false;
			}
			if (fireDelay < 0.0f){
				this.doFire(firer);
				fireDelay += cycleRate;
			}
		}
		else{
			cold = true;
		}
		if (fireDelay < 0f){
			fireDelay = 0f;
		}
		
	}
	
	abstract protected  void doFire(MinEntity firer);
	
}
