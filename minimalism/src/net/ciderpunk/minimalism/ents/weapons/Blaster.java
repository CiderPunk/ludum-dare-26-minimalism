package net.ciderpunk.minimalism.ents.weapons;

import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.minimalism.ents.MinEntity;
import net.ciderpunk.minimalism.ents.missiles.BlasterBullet;
import net.ciderpunk.minimalism.gui.MinimalismGame;

/* simplest weapon fires upwards */
public class Blaster extends Weapon {


	float velocity;
	float direction;
	Vector2 fireVector;
	
	public Blaster(Vector2 offset){
		this(offset, 0f);
	}
	
	
	public Blaster(Vector2 offset, float direction, float delay, float warmUp){
		super(offset, delay, warmUp);
		velocity = 6f;
		this.direction = direction;
		this.fireVector = new Vector2(0,1).rotate(this.direction).scl(velocity);
	}
	
	public Blaster(Vector2 offset, float direction){
		this(offset, direction, 0.1f, 0f);
	}
	
	@Override
	protected void doFire(MinEntity firer) {
		((MinimalismGame) firer.getOwner()).addPlayerProjectile1(new BlasterBullet(firer.getOwner(), new Vector2(firer.getLoc()).add(this.offset), this.fireVector));
	}

}
