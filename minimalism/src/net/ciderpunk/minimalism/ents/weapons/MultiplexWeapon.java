package net.ciderpunk.minimalism.ents.weapons;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.minimalism.ents.MinEntity;

public class MultiplexWeapon extends Weapon {
	
	List<Weapon> weapons;

	public MultiplexWeapon() {
		super(new Vector2(), 0f, 0f);
		weapons = new ArrayList<Weapon>();
	}

	public MultiplexWeapon addWeapon(Weapon b){
		weapons.add(b);
		return this;
	}

	@Override
	public void update(MinEntity firer, Boolean firing, float dT) {
		for (Weapon w : weapons ){
			w.update(firer, firing, dT);
		}	
	}

	@Override
	protected void doFire(MinEntity firer) {}

	
	
}
