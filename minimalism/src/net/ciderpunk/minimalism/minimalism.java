package net.ciderpunk.minimalism;

import net.ciderpunk.minimalism.gui.MinimalismGame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;

public class minimalism implements ApplicationListener {
	
	private MinimalismGame minGame;
	
	@Override
	public void create() {		
		minGame = new MinimalismGame(640,480);
	}


	
	@Override
	public void dispose() {
		minGame.dispose();
	}

	@Override
	public void render(){		
		minGame.update( Gdx.graphics.getDeltaTime() );
	}

	@Override
	public void resize(int width, int height) {
		minGame.resize(width, height);
	}

	@Override
	public void pause() {
		minGame.pauseGame();
	}

	@Override
	public void resume() {
		
	}
}
