package net.ciderpunk.minimalism;

import com.badlogic.gdx.graphics.Color;

public class Constants {
	public static final String SmallFont = "fonts/fixedsys_small.fnt";
	public static final String ScoreFont = "fonts/fixedsys_num_large.fnt";
	public static final String AtlasPath = "atlas/min.atlas";
	
	public static final Color ShadowColor = new Color(0,0,0,0.3f);
	public static final Color DamageFlash = new Color(0.8f,0,0,1f);
	public static final Color FadedColor = new Color(1,1,1,0.3f);
	public static final Color MidFadeColor = new Color(1,1,1,0.7f);
	public static float hurtTime = 0.1f;
	
}
