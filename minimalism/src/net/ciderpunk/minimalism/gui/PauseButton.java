package net.ciderpunk.minimalism.gui;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.input.events.TouchEvent;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.minimalism.Constants;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class PauseButton extends GuiElement implements IResourceUser {
	
	Boolean available;
	static Frame pauseButton;
	
	
	public PauseButton(HorizontalPosition xAnch,	VerticalPosition yAnch, int xOffset, int yOffset) {
		super(48, 48, xAnch, yAnch, xOffset, yOffset);
		available = false;
		// TODO Auto-generated constructor stub
	}
	
	

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		pauseButton = new Frame(atlas.findRegion("pause"),0,0);
	}

	@Override
	public void draw(SpriteBatch batch, int xStart, int yStart) {
		if (available){
			pauseButton.draw(batch, xStart + this.xPos, yStart+this.yPos, Constants.FadedColor );
		}
	}

	
	public boolean onTouch(TouchEvent e, float xRel, float yRel) {
		if (available){
			MinimalismGame game = ((MinimalismGame)this.getRoot());
			if (game.isActive()){
				game.pauseGame();
			}
			else{
				game.resumeGame();
			}
		}		
		e.target = this;
		return true;
	}
	
}
