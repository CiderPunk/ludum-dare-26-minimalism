package net.ciderpunk.minimalism.gui;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.ents.Player;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class HealthBar extends GuiElement implements IResourceUser{

	static Frame outerBar;
	static Frame innerBar;
	MinimalismGame game;

	public HealthBar(int w, int h, HorizontalPosition xAnch,	VerticalPosition yAnch, int xOffset, int yOffset) {
		super(w, h, xAnch, yAnch, xOffset, yOffset);
	}

	@Override
	protected void doDraw(SpriteBatch batch, int x, int y) {
		Player player = game.getPlayer();
		if (player != null){
			float healthWidth =  200f * (player.getHealth() / 100f);
			Color temp = batch.getColor();
			batch.setColor(Constants.MidFadeColor);
			outerBar.draw(batch, x, y);
			if (healthWidth > 0){
				batch.draw((TextureRegion) innerBar, x + 2,  y + 2, healthWidth, 20);
			}
			batch.setColor(temp);
		}
	}

	@Override
	public void attach(GuiElement newParent) {
		super.attach(newParent);
		this.game = (MinimalismGame) this.getRoot();
	}

	@Override
	public void update(float dT) {
		// TODO Auto-generated method stub
		super.update(dT);
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		 resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
		outerBar = new Frame(atlas.findRegion("healthouter"), 0,0);
		innerBar = new Frame(atlas.findRegion("healthinner"),0,0);
	}

	
}
