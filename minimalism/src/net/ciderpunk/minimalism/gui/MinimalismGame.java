package net.ciderpunk.minimalism.gui;



import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.ents.EntityList;
import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.Dialogue;
import net.ciderpunk.gamebase.gui.FpsCounter;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.gui.Theme;
import net.ciderpunk.gamebase.input.events.MouseEvent;
import net.ciderpunk.gamebase.input.events.TouchEvent;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.gamebase.utils.NumberFormatter;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.ents.Explosion;
import net.ciderpunk.minimalism.ents.Player;
import net.ciderpunk.minimalism.ents.missiles.BlasterBullet;
import net.ciderpunk.minimalism.ents.opfor.Potato;
import net.ciderpunk.minimalism.ents.powerup.GreenPower;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

public class MinimalismGame extends GameScreen{

	Player player;
	int widthActual; 
	int heightActual;
	float inputScaleX, inputScaleY;
	static Frame pointer;
	float touchStartX, touchStartY;
	float worldSpeed;

	float difficulty = 1000f;
	EntityList players;
	EntityList playerProjectiles;
	EntityList opFor;
	EntityList opForProjectiles;
	EntityList decorations;
	Boolean gameActive;
	
	Theme theme;

	PauseButton pausebutton;
	ScoreDisplay scoreDisp;
	HealthBar healthDisp;
	
	
	Dialogue dlgStart, dlgPause, dlgGameOver;
	

	/**
	 * gets player entity
	 * @return
	 */
	public Player getPlayer() {
		return player;
	}

	public MinimalismGame(int w, int h) {
		super(w, h);
		this.worldSpeed = -1.0f;

		camera = new OrthographicCamera(640, 480);
	
		players = new EntityList();
		playerProjectiles = new EntityList();
		opFor = new EntityList();
		opForProjectiles= new EntityList();
		decorations = new EntityList();
		gameActive = false;
		
	}

	public float getWorldSpeed() {
		return worldSpeed;
	}


	public EntityList getPlayers() {
		return players;
	}

	public EntityList getPlayerProjectiles() {
		return playerProjectiles;
	}

	public EntityList getOpFor() {
		return opFor;
	}

	public EntityList getOpForProjectiles() {
		return opForProjectiles;
	}
	
	public void pauseGame(){
		this.gameActive = false;
		this.addChild(this.dlgPause);
		
	}
	
	public void resumeGame(){
		this.gameActive = true;
		this.removeChild(this.dlgPause);
	}
	
	public Boolean isActive(){
		return this.gameActive;
	}


	protected void resetGame() {
		this.players.clear();
		this.playerProjectiles.clear();
		this.opFor.clear();
		this.opForProjectiles.clear();
		this.decorations.clear();
		this.addPlayer(this.player = new Player(this,Math.round(this.width * 0.5f), Math.round(this.height * 0.1f)));
		gameActive = true;
		pausebutton.setAvailable(true);
		difficulty = 500f;
	}

	@Override
	public void resize(int width, int height) {		
		this.widthActual = width;
		this.heightActual = height;
		inputScaleX = (float) this.width / (float) width;
		inputScaleY = (float) this.height / (float) height;
	}

	
	public void addPlayerProjectile1(Entity ent){
		this.playerProjectiles.add(ent);
	}
	
	public void addOpForProjectile(Entity ent){
		this.opForProjectiles.add(ent);
	}
	
	public void addPlayer(Entity ent){
		this.players.add(ent);
	}
	
	public void addOpFor(Entity ent){
		this.opFor.add(ent);
	}
	
	public void addDecoration(Entity ent){
		this.decorations.add(ent);
	}
	
	@Override
	public void think(float dT) {
		if (gameActive){
			this.players.doUpdate(dT);
			this.opFor.doUpdate(dT);
			this.playerProjectiles.doUpdate(dT);
			this.opForProjectiles.doUpdate(dT);
			this.decorations.doUpdate(dT);
			difficulty += (dT * 5f);
			if (MathUtils.random(0, 10000) < difficulty){
				this.addOpFor(new Potato(this, MathUtils.random(0, this.width), this.height + 50));
			}
		}
	}

	@Override
	public void doDraw(SpriteBatch batch, int x, int y) {
		Gdx.gl.glClearColor(0.1f, 0.6f, 0.1f, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		this.opFor.doDraw(batch);
		this.players.doDraw(batch);
		this.opForProjectiles.doDraw(batch);
		this.playerProjectiles.doDraw(batch);
		this.decorations.doDraw(batch);
		super.doDraw(batch, x, y);
		batch.end();
	}


	
	@Override
	public float translateTouchX(float xTouch) {
		return  xTouch * this.inputScaleX;
	}

	@Override
	public float translateTouchY(float yTouch) {
		return  yTouch * this.inputScaleY;
	}

	public void setGameOver() {
		pausebutton.setAvailable(false);
		this.addChild(this.dlgGameOver);
		this.dlgGameOver.setMessage("You have died, again, your final score was " + NumberFormatter.formatNumber(this.player.getScore()) + ".\n\nThanks for playing!\n -CiderPunk \n\nLudum Dare #26 April 2013.");
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		this.theme = Theme.LoadTheme("theme.json");
		
		resMan.addResourceUser(this.theme);
	
		this.addChild(new FpsCounter(HorizontalPosition.Right,	VerticalPosition.Top, 10, 10, Constants.SmallFont ));
		this.addChild(new ScoreDisplay(HorizontalPosition.Mid, VerticalPosition.Top, 0, 10));
		this.addChild(new HealthBar(204, 22,  HorizontalPosition.Left, VerticalPosition.Bottom, 10,10));
		this.addChild(pausebutton = new PauseButton(HorizontalPosition.Left, VerticalPosition.Top, 10, 10));
		
		IResourceUser[] ents = { new Player(), new BlasterBullet(), new Potato() , new Explosion(), new GreenPower()};
		resMan.addResourceUser(ents);
		super.preLoad(resMan);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		super.resize(this.width, this.height);
	}


	@Override
	public void startGame() {
		this.dlgStart = (Dialogue) new Dialogue(400, 200, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme, "Welcome to 2755, You are Piet Mondrian, the famous De Stijl artist, bought back from the dead by an advanced culture of Minimalist followers desperate to defend their ways from a militant faction of Stuckists. You are the only thing standing betweem humanity and an era of tedious figurative art. Watch out, they have potatoes!\n Good luck out there!", "Start Game!");
		this.dlgStart.addListener(new IListener(){
			public void doEvent(Object source){ 
	  		self.removeChild((GuiElement)source);
	  		((MinimalismGame) self).resetGame();
	  		System.out.println("dialogue!");
	  	}
		});

		this.dlgPause = (Dialogue) new Dialogue(200, 100, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme, "Paused", "Continue");
		dlgPause.addListener(new IListener(){
	  	public void doEvent(Object source){ 
	  		self.removeChild((GuiElement) source);
	  		((MinimalismGame) self).resumeGame();
	  	}
	  });
		
		this.dlgGameOver = (Dialogue) new Dialogue(400, 200, HorizontalPosition.Mid, VerticalPosition.Mid, 0, 0, theme,"You have died!", "Try again");
	  dlgGameOver.addListener(new IListener(){
	  	public void doEvent(Object source){ 
	  		self.removeChild((GuiElement) source);
	  		((MinimalismGame) self).resetGame();
	  	}
	  });
	

		this.addChild(this.dlgStart);
	}

	@Override
	public void draw(SpriteBatch batch, int xStart, int yStart) {
		doDraw(batch, xStart + this.xPos, yStart + this.yPos);
	}
	
	

	public boolean onTouch(TouchEvent e, float xRel, float yRel) {
		if (player!= null){
			player.startFire();
		}
		e.target = this;
		return true;
	}
		
	
	
	/*
	
	@Override
	public void cancelTouch() {
		this.release();
	}
*/
	@Override
	public boolean onMouseMove(MouseEvent e, float xRel, float yRel) {
		e.target = this;
		return true; 
	}

	@Override
	public boolean onDrag(TouchEvent e) {
		super.onDrag(e);
		if (this.player!= null){
			player.updateTarget(e.dX, e.dY);
		}
		return true;
	}

	@Override
	public boolean onRelease(TouchEvent e) {
		if (this.player!= null){
			this.player.ceaseFire();
		}
		return true;
	}


	
	
	
	
}
