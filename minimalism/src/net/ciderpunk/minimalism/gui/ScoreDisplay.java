package net.ciderpunk.minimalism.gui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.gamebase.utils.NumberFormatter;
import net.ciderpunk.minimalism.Constants;
import net.ciderpunk.minimalism.ents.Player;

public class ScoreDisplay extends GuiElement implements IResourceUser{

	static BitmapFont scoreFont;
	String scoreText;
	long lastScore;

	public ScoreDisplay(){}

	public ScoreDisplay(HorizontalPosition xAnch,	VerticalPosition yAnch, int xOffset, int yOffset) {
		super(10, 32, xAnch, yAnch, xOffset, yOffset);
		lastScore = -1;
		scoreText = "";
	}


	
	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(Constants.ScoreFont, BitmapFont.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		scoreFont = resMan.getAssetMan().get(Constants.ScoreFont, BitmapFont.class);
	}
	
	@Override
	public void update(float dT) {
		Player player = ((MinimalismGame)this.getRoot()).getPlayer();
		if (player!= null){
			if (player.getScore() != lastScore){
				lastScore = player.getScore();
				scoreText = NumberFormatter.formatNumber(lastScore);
				TextBounds bounds = scoreFont.getBounds(scoreText);
				this.width = (int) Math.ceil(bounds.width); 
				this.height = (int) Math.ceil(bounds.height);
				this.updatePosition();
			}
		}
	}

	@Override
	public void draw(SpriteBatch batch, int xStart, int yStart) {
		scoreFont.draw(batch, scoreText, xStart + this.xPos, yStart + this.yPos + this.height);
	}

}
