package net.ciderpunk.packer;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;
public class MyPacker {
  public static void main (String[] args) throws Exception {
  	Settings settings = new Settings();
  	settings.maxWidth = 512;
  	settings.maxHeight = 512;
    TexturePacker2.process(settings, "C:/Users/Matthew/Documents/minimalism ld26/atlas", "C:/Users/Matthew/Documents/minimalism ld26/minimalism-android/assets/atlas", "min");
  }
}