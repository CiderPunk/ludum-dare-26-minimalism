package net.ciderpunk.gamebase.utils;

public class NumberFormatter {
	public static String formatNumber(long num){
		Boolean comma = false;
		String temp = Long.toString(num);
		String formatted = "";
		int start = temp.length() % 3;
		if (start > 0){
			formatted = temp.substring(0,start);
			comma = true;
		}
		else{
			formatted = "";
		}
		
		for(int i = start; i < temp.length(); i+=3){
			formatted += (comma ? "," : "") +temp.substring(i, i+3);
			comma = true;
		}
		return formatted;
	}
}
