package net.ciderpunk.gamebase.ents;

import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class EntityList {

	protected LinkedList<Entity> mainBuffer;
	protected LinkedList<Entity> addBuffer;
	
	public EntityList() {
		addBuffer = new LinkedList<Entity>();
		mainBuffer = new LinkedList<Entity>();
	}

	public void clear(){
		addBuffer.clear();
		mainBuffer.clear();
	}
	
	public void consolidateList(){
		if (!addBuffer.isEmpty()){
			mainBuffer.addAll(addBuffer);
			addBuffer.clear();
		}
	}
		
	/** Performs update on all members of the list
	 * @param dT delta time
	 */
	public EntityList doUpdate(float dT){
		this.consolidateList();
		Iterator<Entity> i = mainBuffer.iterator();
		while(i.hasNext()){
			Entity e = i.next();
			if (!e.update(dT)){
				i.remove();
			}
		}
		return this;
	}

	/**
	 * perform draw operation on all members of the list
	 * @param batch
	 */
	public void doDraw(SpriteBatch batch){
		Iterator<Entity> i = mainBuffer.iterator();
		while(i.hasNext()){
			i.next().draw(batch);
		}
	}
	
	public Entity pointCollisonTest(Vector2 loc, float distance){
		Iterator<Entity> i = mainBuffer.iterator();
		while(i.hasNext()){
			Entity e = i.next();
			if (e.pointCollisoonTest(loc, distance)){
				return e;
			}
		}
		return null;
	}
	
	public boolean add(Entity arg0) {
		return addBuffer.add(arg0);
	}

}
