package net.ciderpunk.gamebase.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GameScreen;


/**
 * @author Matthew
 *
 */
public abstract class Entity {

	protected GameScreen owner;
	protected Vector2 loc;
	protected Frame currentFrame;
	static Vector2 distanceTest = new Vector2();
	
	
	protected abstract float getCollisionRadius();
	
	/**
	 * constructor
	 * @param owner
	 */
	public Entity(GameScreen owner){
		this.owner = owner;
		loc = new Vector2(); 
	}
	
	public Entity(GameScreen owner, int x, int y){
		this(owner);
		this.loc.set(x,y);
	}
	
	public Entity(GameScreen owner, Vector2 loc){
		this(owner);
		this.loc.set(loc);
	}
	
	public Entity(){
		this(null);
	}
	
	public GameScreen getOwner() {
		return owner;
	}


	public Vector2 getLoc() {
		return loc;
	}

	public Boolean canCollide(){
		return false;
	}

	public void draw(SpriteBatch batch){
		currentFrame.draw(batch, loc.x, loc.y);
	}
	
	public abstract Boolean update(float dT);
	
	public boolean pointCollisoonTest(Vector2 target, float distance) {
		float maxDist = distance + this.getCollisionRadius();
		distanceTest.set(this.loc).sub(target);
		if (Math.abs(distanceTest.x) < maxDist && Math.abs(distanceTest.y) < maxDist){
			return distanceTest.len2() < (maxDist * maxDist);
		}
		return false;
	}

	public void dispose() {

	}
	

	
}
