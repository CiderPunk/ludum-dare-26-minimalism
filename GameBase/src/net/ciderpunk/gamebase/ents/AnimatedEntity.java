package net.ciderpunk.gamebase.ents;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GameScreen;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public abstract class AnimatedEntity extends Entity {

	
	public AnimatedEntity() {
		super();
	}
	
	public AnimatedEntity(GameScreen owner, int x, int y) {
		super(owner, x, y);
	}


	public AnimatedEntity(GameScreen owner, Vector2 loc) {
		super(owner, loc);
	}


	protected float stateTime;
	protected Animation currentAnim;
	
	protected void setAnim(Animation anim){
		if (currentAnim != anim){
			stateTime = 0f;
			currentAnim = anim;
		}
	}
	
	@Override
	public Boolean update(float dT) {
		this.stateTime += dT;
		this.currentFrame = (Frame)currentAnim.getKeyFrame(this.stateTime, true);
		return true;
	}

	public Animation buildAnim(TextureAtlas atlas, String name, float frameDelay, int xOff, int yOff){
		Array<AtlasRegion> regions = atlas.findRegions(name);
		Frame[] frames = new Frame[regions.size];
		for(int i = 0; i < regions.size; i++){
			frames[i] = new Frame(regions.get(i), xOff, yOff);
		}
		return new Animation(frameDelay, frames);
	}

}
