package net.ciderpunk.gamebase.events;


public interface IEventCaller<T> {
	T addListener(IListener listener);
}
