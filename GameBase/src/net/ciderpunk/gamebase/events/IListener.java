package net.ciderpunk.gamebase.events;

public interface IListener {
	void doEvent(Object source);
}
