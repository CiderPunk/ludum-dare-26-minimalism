package net.ciderpunk.gamebase.events;

import java.util.LinkedList;

public class EventCaller {

	LinkedList<IListener> listeners;
	
	public void addListener(IListener listener){
		if (listeners == null){
			listeners = new LinkedList<IListener>();
		}
		listeners.add(listener);	
	}

	public void doEvent(Object source) {
		if (listeners != null){
			for (IListener listener : listeners){		
				listener.doEvent(source);
			}
		}
	}
	
	
}
