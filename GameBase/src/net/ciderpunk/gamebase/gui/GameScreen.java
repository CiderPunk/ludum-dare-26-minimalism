package net.ciderpunk.gamebase.gui;

import net.ciderpunk.gamebase.input.GameGestureListener;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;

/**
 * @author Matthew
 *
 */
public abstract class GameScreen extends GuiElement implements IResourceUser{
	

	protected OrthographicCamera camera;
	protected TextureAtlas atlas;
	protected GameGestureListener inputListener;
	protected SpriteBatch batch;
	protected ResourceManager resMan;
	
	protected final GameScreen self = this;
	
	public OrthographicCamera getCamera() {
		return camera;
	}


	public GameScreen(int w, int h) {
		super(w, h, HorizontalPosition.Left, VerticalPosition.Top, 0, 0 );
		inputListener = new GameGestureListener(this); 
		Gdx.input.setInputProcessor(new GestureDetector(inputListener));
		
		float screenWidth = Gdx.graphics.getWidth();
		float screenHeight = Gdx.graphics.getHeight();
		camera = new OrthographicCamera(1, screenWidth / screenHeight);
		batch = new SpriteBatch();
		resMan = new ResourceManager(this);
	}


	@Override
	public void updatePosition() {
		//do nothing, these buggers dont move
	}

	
	public void resize(int width, int height){
		super.resize(width, height);
		camera.setToOrtho(false, width, height);
	}
	
	public void dispose(){
		batch.dispose();
		resMan.dispose();
	}


	public void update(float dT){
		if (resMan.update()){
			this.draw(this.batch,0,0);
			inputListener.update();
			this.think(dT);
			super.update(dT);
		}
	}
	
	abstract protected void think(float dT);

	@Override
	public void draw(SpriteBatch batch, int xStart, int yStart) {
		super.draw(batch, xStart, yStart);
	}


	public GuiElement touched(float x, float y, int pointer,int button) {
		return this.testTouch((int)x,this.height-(int)y,pointer,button);
	}
	
	
	public float translateTouchX(float xTouch){
		return xTouch;		
	}

	public float translateTouchY(float yTouch){
		return yTouch;		
	}

	public Boolean checkBounds(Vector2 loc) {
		return this.checkBounds(loc, 20);
	}
	
	public Boolean checkBounds(Vector2 loc, int margin) {
		return loc.x > -margin && loc.x < this.width + margin && loc.y > -margin && loc.y < this.height+ margin;
	}
	
	public abstract void startGame();
}