package net.ciderpunk.gamebase.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
public abstract  class GuiElement implements IResourceUser {

	@Override
	public void preLoad(ResourceManager resMan) {
		if (children != null){
			for (GuiElement child : this.children){
				resMan.addResourceUser(child);				
			}
		}
	}

	@Override
	public void postLoad(ResourceManager resMan) {
	}

	public enum VerticalPosition{ Top, Bottom, Mid }
	public enum HorizontalPosition{ Left, Right, Mid }

	protected VerticalPosition yAnchor;
	protected HorizontalPosition xAnchor;
	protected int width,height;
	protected int xOffs, yOffs;
	protected int xPos, yPos;
	protected List<GuiElement> children = null;
	protected GuiElement parent;
	
	public void addChild(GuiElement element){
		if (children == null){
			children = new ArrayList<GuiElement>();
		}
		children.add(element);
		element.attach(this);
	}
	
	public void removeChild(GuiElement element){
		if (children != null){
			children.remove(element);
		}
		element.detach();
		
	}
	
	public void attach( GuiElement newParent){
		this.parent = newParent;
		this.updatePosition();
		if (children!= null){
			for (GuiElement child : children){
				child.attach(this);
			}
		}
	}
	
	public void detach(){
		this.parent = null;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	
	public GuiElement getRoot(){
		return (this.parent == null ? this : this.parent.getRoot());
	}
	
	public GuiElement getParent() {
		return parent;
	}

	public GuiElement(){
		this(0,0, HorizontalPosition.Left, VerticalPosition.Top,0,0);
	}
	
	public GuiElement(int w, int h, HorizontalPosition xAnch, VerticalPosition yAnch, int xOffset, int yOffset){
		this.parent = null;
		this.width = w; 
		this.height = h;
		this.xAnchor = xAnch;
		this.yAnchor = yAnch;
		this.xOffs = xOffset;
		this.yOffs = yOffset;
	}
	
	public void resize(int width, int height){
		this.width = width;
		this.height = height;
		if (this.children != null){
			for(GuiElement child : this.children){
				child.updatePosition();
			}
		}
	}
	
	public void updatePosition(){
		if (parent  != null){
			switch(this.xAnchor){
				case Left:
					this.xPos = this.xOffs;
					break;
				case Right:
					this.xPos = parent.getWidth() - this.xOffs - this.width;;
					break;
				case Mid:
					this.xPos = (parent.getWidth() - this.width) / 2;
					break;
			}
			switch(this.yAnchor){
				case Bottom:
					this.yPos = this.yOffs;
					break;
				case Top:
					this.yPos = parent.getHeight() - this.yOffs -this.height;
					break;
				case Mid:
					this.yPos = (parent.getHeight() - this.height) / 2;
					break;
			}
		}
	}
	
	public void draw(SpriteBatch batch, int xStart, int yStart){
		doDraw(batch, xStart + this.xPos, yStart + this.yPos);
	}
	
	
	protected void doDraw(SpriteBatch batch, int x, int y){
		drawChildren(batch, x, y);
	}
	
	public void drawChildren(SpriteBatch batch, int xstart, int ystart){
		if (children != null && !children.isEmpty()){
			Iterator<GuiElement> i = children.iterator();
			while(i.hasNext()){
				i.next().draw(batch, xstart, ystart);
			}
		}
	}
	
	public void update(float dT){
		updateChildren(dT);	
	}
	
	public void updateChildren(float dT){
		if (children != null && !children.isEmpty()){
			Iterator<GuiElement> i = children.iterator();
			while(i.hasNext()){
				i.next().update(dT);
			}
		}
	}



	public GuiElement doDrag(float dX, float dY){
		return this;
	}
	
	public void cancelTouch(){
		//element is no longer the subject of a touch,
		
	}
	
	public void release(){
		//mouse button released or touch removed.. trigger button press etc.
	}
	
	public GuiElement doTouch(float x, float y, int pointer, int button){
		GuiElement touched = null;
		if (children!= null){
			for(GuiElement element : children){
				if ((touched = element.testTouch(x,y,pointer,button)) != null){
					break;
				}
			}
		}
		return touched;
	}

	
	/**
	 * checks if the point is within the bounds of this element 
	 * @param x
	 * @param y
	 * @return true on contained
	 */
	protected Boolean contains(float x, float y){
		return (x > 0 && x < this.width && y > 0 && y < this.height);
	}
	
	public GuiElement testTouch(float x, float y, int pointer, int button) {
		float xRel = x - this.xPos;
		float yRel = y -  this.yPos;
		if (contains(xRel, yRel)){
			return doTouch(xRel, yRel, pointer,button);	
		}
		return null;
	}


}
