package net.ciderpunk.gamebase.gui;

public abstract class ThemedElement extends GuiElement {

	protected Theme theme;
	
	public ThemedElement(int w, int h,	HorizontalPosition xAnch, VerticalPosition yAnch, int xOffset, int yOffset, Theme theme) {
		super(w, h, xAnch, yAnch, xOffset, yOffset);
		// TODO Auto-generated constructor stub
		this.theme = theme;
	}

}
