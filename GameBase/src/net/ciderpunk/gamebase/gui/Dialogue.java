package net.ciderpunk.gamebase.gui;

import net.ciderpunk.gamebase.events.EventCaller;
import net.ciderpunk.gamebase.events.IEventCaller;
import net.ciderpunk.gamebase.events.IListener;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class Dialogue extends ThemedElement implements IListener, IEventCaller<GuiElement> {

	protected String message;
	EventCaller caller;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public Dialogue(int w, int h, HorizontalPosition xAnch,
			VerticalPosition yAnch, int xOffset, int yOffset, Theme theme, String message, String buttonText) {
		super(w, h, xAnch, yAnch, xOffset, yOffset, theme);
		this.theme = theme.getTheme("panel");
		this.message = message;
		this.caller = new EventCaller();
		
		TextButton btn = new TextButton(0, 0, HorizontalPosition.Right, VerticalPosition.Bottom, this.theme.getRightPad(), this.theme.getBottomPad(), buttonText, theme);
		btn.addListener(this);
		this.addChild(btn);
		caller = new EventCaller();
	}

	@Override
	public void doEvent(Object source) {
		caller.doEvent(this);
	}

	@Override
	public void doDraw(SpriteBatch batch, int x, int y) {
		theme.getPatch().draw(batch, x, y, this.width, this.height);
		theme.getFont().drawWrapped(batch, this.getMessage(), x + theme.getLeftPad(), y + this.height - theme.getTopPad(), this.width - theme.getLeftPad() - theme.getRightPad());
		super.doDraw(batch, x, y);
	}

	@Override
	public GuiElement addListener(IListener listener) {
		this.caller.addListener(listener);
		return this;
	}

}
