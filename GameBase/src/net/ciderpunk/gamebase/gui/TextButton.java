package net.ciderpunk.gamebase.gui;

import java.util.LinkedList;

import net.ciderpunk.gamebase.events.EventCaller;
import net.ciderpunk.gamebase.events.IEventCaller;
import net.ciderpunk.gamebase.events.IListener;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TextButton extends ThemedElement implements IEventCaller<GuiElement> {

	EventCaller caller;
	Boolean fixedWidth, fixedHeight;
	String text, fontPath;
	BitmapFont font;
	float touchDownX, touchDownY;
	int margin;
	Boolean focused;
	int textX, textY;
	Theme pressedTheme;

	
	public TextButton(int w, int h, HorizontalPosition xAnch, VerticalPosition yAnch, int xOffset, int yOffset, String text, Theme theme) {
		super(w, h, xAnch, yAnch, xOffset, yOffset, theme);

		this.pressedTheme = theme.getTheme("buttonPressed");
		this.theme = theme.getTheme("buttonNormal");
		
		fixedWidth = (w != 0);
		fixedHeight = (h!=0);
		this.text = text;
		this.focused = false;
		caller = new EventCaller();
		refreshSize();
	}

	public void setText(String text)
	{
		this.text = text;
		refreshSize();
	}
	
	public String getText()
	{
		return this.text;
	}

	protected void refreshSize(){
		TextBounds bounds = theme.getFont().getBounds(this.text);
		if (!this.fixedHeight){
			this.height = (int) Math.ceil(theme.getFont().getLineHeight() + this.theme.getTopPad() + this.theme.getBottomPad());
			textY = (int) Math.ceil(this.height - this.theme.getTopPad());
		}
		if (!this.fixedWidth){
			this.width = (int) Math.ceil(bounds.width + this.theme.getLeftPad() + this.theme.getRightPad());
			textX = theme.getLeftPad();
		}
		this.updatePosition();
	}

	@Override
	public void doDraw(SpriteBatch batch, int x, int y) {
		if (focused){
			this.pressedTheme.getPatch().draw(batch, x, y, this.width, this.height);
		}
		else{
			this.theme.getPatch().draw(batch, x, y, this.width, this.height);
		}	
		theme.getFont().draw(batch, this.getText(), x + textX, y + textY);
	}

	@Override
	public GuiElement doTouch(float x, float y, int pointer, int button) {
		if (pointer == 0 && button == 0){
			this.touchDownX = x;
			this.touchDownY = y;
			this.focused = true;
			System.out.println("CLICK:" +this.touchDownX + "," + this.touchDownY);
			return this;
		}
		return null;
	}
	
	@Override 
	public GuiElement doDrag(float dX, float dY){
		this.touchDownX+=dX;
		this.touchDownY-=dY;
		focused = this.contains(this.touchDownX, this.touchDownY);	
		System.out.println("drag:" +this.touchDownX + "," + this.touchDownY);
		return this;
	}

	@Override
	public void cancelTouch() {
		focused = false;
	}

	@Override
	public void release() {
		if (focused){
			this.caller.doEvent(this);
		}
		focused = false;
	}

	@Override
	public GuiElement addListener(IListener listener) {
		this.caller.addListener(listener);
		return this;
	}
	

	
	
}
