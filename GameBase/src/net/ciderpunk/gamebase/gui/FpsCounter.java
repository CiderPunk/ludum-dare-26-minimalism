package net.ciderpunk.gamebase.gui;

import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;

public class FpsCounter extends GuiElement implements IResourceUser {
	String fontName;
	BitmapFont font;
	long nextTime;
	int frames;
	int lastFrames;
	Boolean visible;

	@Override
	public GuiElement doTouch(float x, float y, int pointer, int button) {
		this.visible = ! this.visible;
		return this;
	}

	public FpsCounter(String fontPath) { super(); this.fontName = fontPath; }
	
	public FpsCounter(HorizontalPosition xAnch,	VerticalPosition yAnch, int xOffset, int yOffset, String font) {
		super(50, 50, xAnch, yAnch, xOffset, yOffset);		
		this.fontName = font;
		nextTime = TimeUtils.millis() + 1000;
		frames = 0;
		lastFrames = 0;
		visible = false;
	}

	@Override
	public void draw(SpriteBatch batch, int xStart, int yStart) {
		if (this.visible){
			this.font.draw(batch, Integer.toString(lastFrames) + "FPS", xStart + this.xPos, yStart + this.yPos + this.height);	
			frames++;
		}
	}

	@Override
	public void update(float dT) {
		// TODO Auto-generated method stub
		super.update(dT);
		if (TimeUtils.millis() > nextTime){
			lastFrames = frames;
			frames = 0;
			nextTime = TimeUtils.millis() + 1000;
		}
	}

	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(this.fontName, BitmapFont.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		font = resMan.getAssetMan().get(this.fontName, BitmapFont.class);
		
		TextBounds bounds = font.getBounds("999FPS");
		this.resize((int)bounds.width, (int)bounds.height);
		this.updatePosition();
	}
}
