package net.ciderpunk.gamebase.gui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Slider extends  ThemedElement {
	
	public enum Direction{
		Horizontal,
		Vertical,
	}
	
	public class SliderWidget extends ThemedElement{

		protected SliderWidget(Theme theme) {
			super(0, 0, HorizontalPosition.Left, VerticalPosition.Top, 0, 0, theme);

			this.theme = theme.getTheme("widget");
		}

		@Override
		protected void doDraw(SpriteBatch batch, int x, int y) {
			// TODO Auto-generated method stub
			theme.getPatch().draw(batch, x, y, this.width, this.height);
		}

		
		
	}
	
	
	protected float widgetSize;
	protected SliderWidget widget;
	protected int positions;
	protected Direction direction;
	
	
	public Slider(int w, int h, HorizontalPosition xAnch, VerticalPosition yAnch,
			int xOffset, int yOffset, Theme theme, Slider.Direction dir, int positions, float widgetSize ) {
		super(w, h, xAnch, yAnch, xOffset, yOffset, theme);
		this.widgetSize = widgetSize; 
		this.positions = positions;
		this.theme = theme.getTheme("slider");		
		this.widget = new SliderWidget(this.theme);
		this.direction = dir;
	}

	
	
	
	@Override
	protected void doDraw(SpriteBatch batch, int x, int y) {
		theme.getPatch().draw(batch, x, y, this.width, this.height);
		super.doDraw(batch, x, y);
	}




	protected void updateWidget(){
		switch(this.direction){
			case Horizontal:
					this.widget.resize((int)Math.floor((float)this.width * this.widgetSize), this.height);
				break;
			case Vertical:
			
				break;
		}
		
		
	}



}
