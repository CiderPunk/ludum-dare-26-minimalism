package net.ciderpunk.gamebase.resources;
public interface IResourceUser {
	void preLoad(ResourceManager resMan);
	void postLoad(ResourceManager resMan);
	
}
