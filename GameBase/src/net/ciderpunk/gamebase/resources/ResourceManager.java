package net.ciderpunk.gamebase.resources;

import java.util.Iterator;
import java.util.LinkedList;

import net.ciderpunk.gamebase.gui.GameScreen;

import com.badlogic.gdx.assets.AssetManager;

public class ResourceManager  {

	AssetManager assetMan;
	LinkedList<IResourceUser> postLoadList;
	boolean loading;
	GameScreen owner;


	public AssetManager getAssetMan() {
		return assetMan;
	}


	public void setAssetMan(AssetManager assetMan) {
		this.assetMan = assetMan;
	}


	public ResourceManager(GameScreen owner){
		assetMan = new AssetManager();
		postLoadList = new LinkedList<IResourceUser>();
		this.owner = owner;	
		this.addResourceUser(owner);
	}
	
	
	public boolean update(){
		if (assetMan.update()) {
			if (loading){
				loading = false;
				Iterator<IResourceUser> i = postLoadList.iterator();
				while(i.hasNext()){
					IResourceUser item = i.next();
					item.postLoad(this);
					i.remove();
				}
				owner.startGame();
				//we start proper next time!
				return false;
			}
			return true;
		}
		return false;
	}

	
	public void dispose(){
		assetMan.dispose();
	}
	
	public void addResourceUser(IResourceUser object){
		postLoadList.add(object);
		object.preLoad(this);

		loading = true;
	}
	
	public void addResourceUser(IResourceUser[] objects){
		for(IResourceUser object : objects){
			addResourceUser(object);
		}
	}

}
