package net.ciderpunk.gamebase.input;

import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.gui.GuiElement;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

public class GameGestureListener implements GestureListener{


	static final int maxPointers = 5;
	float lastX, lastY;
	
	GameScreen game;
	GuiElement[] targets;

	public GameGestureListener(GameScreen game){
		
		this.game = game;
		targets = new GuiElement[maxPointers];
		//targets = new Array<GuiElement>();
		for (int i = 0; i < maxPointers; i++){
			targets[i] = null;
		}
	}

	
	public void update(){
		for (int i = 0; i < maxPointers; i++){
			if (targets[i] != null){
				if (!Gdx.input.isTouched(0)){
					targets[i].release();
					targets[i] = null;;
				}
			}
		}
	}
	
	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		targets[pointer] = game.touched(game.translateTouchX(x), game.translateTouchY(y), pointer, button);
		if (pointer == 0 && button == 0){
			lastX = x; lastY = y;
		}
		return true;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		//dx and dy are generally wrong!
		deltaX = x - lastX;
		deltaY = y - lastY;
		lastX = x;
		lastY = y;
		boolean found = false;
		for (int i = 0; i < maxPointers; i++){
			if (targets[i] != null){
				if(found){
					targets[i].cancelTouch();
					targets[i] = null;
				}
				else{
					targets[i] = targets[i].doDrag(game.translateTouchX(deltaX),game.translateTouchY(deltaY));
					found = true;
				}
			}
		}
		return true;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}

}
